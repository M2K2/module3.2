using System;
using System.Collections.Generic;

namespace Module3_2
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("It's lit!");
        }
    }

    public class Task4
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            if (Int32.TryParse(input, out result) || result > 0)
                return false;
            else return true;
        }

        public int[] GetFibonacciSequence(int n)
        {
            int[] array = new int[n];

            if (n == 0)
                return array;
            array[0] = 0;
            if (n == 1)
                return array;
            array[1] = 1;
            for (int i = 2; i < n; i++)
            {
                array[i] = array[i - 1] + array[i - 2];
            }
            return array;
        }
    }

    public class Task5
    {
        public int ReverseNumber(int sourceNumber)
        {
            bool minus = false;
            if (sourceNumber < 0)
            {
                minus = true;
                sourceNumber *= (-1);
            }
            string s = "";
            for (int i = sourceNumber.ToString().Length; i > 0; i--)
            {
                s += sourceNumber.ToString()[i - 1];
            }
            if (minus == true)
            {
                return Convert.ToInt32('-' + s);
            }
            else
            {
                return Convert.ToInt32(s);
            }
        }
    }

    public class Task6
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            int[] array = null;

            if (size < 0)
                return array;
            Random random = new Random();
            array = new int[size];
            for (int i = 0; i < size; i++)
            {
                array[i] = random.Next(-10, 10);
            }
            return array;
        }

        public int[] UpdateElementToOppositeSign(int[] source)
        {
            for(int i=0;i<source.Length; i++)
            {
                if(source[i]!=0)
                {
                    source[i] *= -1;
                }
            }
            return source;
        }
    }

    public class Task7
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            int[] array = null;

            if (size < 0)
                return array;
            Random random = new Random();
            array = new int[size];
            for (int i = 0; i < size; i++)
            {
                array[i] = random.Next(-10, 10);
            }
            return array;
        }

        public List<int> FindElementGreaterThenPrevious(int[] source)
        {
            List<int> list = new List<int>();
            for (int i = 1; i < source.Length; i++)
            {
                if (source[i] > source[i - 1])
                {
                    list.Add(source[i]);
                }
            }
            return list;
        }
    }

    public class Task8
    {
        public int[,] FillArrayInSpiral(int size)
        {
            throw new NotImplementedException();
        }
    }
}